using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using Unity.VisualScripting;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    private float destructTimer = 1.0f;
    private bool bulletTracked = false;
    private GameObject bulletGhostPrefab;

    // Start is called before the first frame update
    void Start()
    {
        bulletGhostPrefab = Resources.Load<GameObject>("BulletGhost");
    }

    // Update is called once per frame
    void Update()
    {
        destructTimer -= Time.deltaTime;
        if(destructTimer <= 0)
            GameObject.Destroy(gameObject);

        if (bulletTracked)
            Instantiate(bulletGhostPrefab, transform.position, transform.rotation);
    }

    public void TrackBullet()
    {
        bulletTracked = true;
    }
}
