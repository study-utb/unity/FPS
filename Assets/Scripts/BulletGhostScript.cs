using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGhostScript : MonoBehaviour
{
    private float destructTimer = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        destructTimer -= Time.deltaTime;
        if(destructTimer <= 0)
            GameObject.Destroy(gameObject);
    }
}
