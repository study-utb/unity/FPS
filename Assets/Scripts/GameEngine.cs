using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEditor.MemoryProfiler;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameEngine : MonoBehaviour
{
    public TextMeshProUGUI EnemyCounter;
    public TextMeshProUGUI CasualtiesCounter;

    private int enemies = 0;
    private int friends = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        UpdateCounters();
        CanScript[] cans = (CanScript[])GameObject.FindObjectsOfType(typeof(CanScript));
        foreach (CanScript can in cans)
        {
            can.Hit += Hit;
        }
    }

    private void Hit(object sender, EventArgs e)
    {
        CanScript cs = sender as CanScript;
        if (cs != null)
        {
            if (cs.TCanType == CanScript.CanType.enemy)
                enemies++;
            if (cs.TCanType == CanScript.CanType.friendly)
                friends++;
            UpdateCounters();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateCounters()
    {
        EnemyCounter.text = String.Format("Nepřítelé: {0}", enemies);
        CasualtiesCounter.text = String.Format("Ztráty: {0}", friends);
    }
}
