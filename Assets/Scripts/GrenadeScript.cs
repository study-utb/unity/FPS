using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class GrenadeScript : MonoBehaviour
{
    private float destructTimer = 1f;
    private bool exploded = false;

    private Rigidbody rigidBody;

    private void Awake()
    {
        rigidBody = gameObject.GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        destructTimer -= Time.deltaTime;
        if(destructTimer <= 0)
            GameObject.Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!exploded)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, 1.8f);
            foreach (Collider hit in colliders)
            {
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if(rb != null)
                    rb.AddExplosionForce(3, transform.position, 2.3f, 0.5f, ForceMode.Impulse);
                CanScript cs = hit.GetComponent<CanScript>();
                if(cs != null)
                    cs.Kill();
            }
            
            exploded = true;
        }
        GameObject.Destroy(gameObject);
    }
}
