using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanScript : MonoBehaviour
{
    public enum CanType
    {
        neutral = 0,
        enemy,
        friendly
    }

    public CanType TCanType = CanType.neutral;
    public Material FriendlyMat;
    public Material EnemyMat;
    public Material NeutralMat;

    public EventHandler Hit;

    private MeshRenderer mr;

    private void Awake()
    {
        mr = gameObject.GetComponent<MeshRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if( TCanType == CanType.friendly )
            mr.material = FriendlyMat;
        if (TCanType == CanType.enemy)
            mr.material = EnemyMat;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        BulletScript bs = collision.gameObject.GetComponent<BulletScript>();
        GrenadeScript gs = collision.gameObject.GetComponent<GrenadeScript>();

        if (bs != null || gs != null)
            Kill();

    }

    public void Kill()
    {
        Hit?.Invoke(this, EventArgs.Empty);
        mr.material = NeutralMat;
        TCanType = CanType.neutral;
    }
}
