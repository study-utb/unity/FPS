using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class PlayerScript : MonoBehaviour
{
    private GameObject gun;
    private GameObject muzzle;
    private GameObject bulletPrefab;
    private GameObject grenadePrefab;
    private Rigidbody rigidBody;

    private void Awake()
    {
        gun = GameObject.Find("GunHandler");
        muzzle = GameObject.Find("Muzzle");
        bulletPrefab = Resources.Load<GameObject>("Bullet");
        grenadePrefab = Resources.Load<GameObject>("Grenade");
        rigidBody = gameObject.GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float speed = 2f;
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, (Input.GetKey(KeyCode.LeftShift) ? 3 : 1) * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }
        
        if(Input.GetKeyDown(KeyCode.Space))
            rigidBody.AddForce(0, 7000, 0);

        float horizontal = Input.GetAxis("Mouse X");
        transform.Rotate(0, horizontal, 0);
        float vertical = -Input.GetAxis("Mouse Y");
        
        if (CanViewChange(vertical))
            gun.transform.Rotate(vertical, 0, 0);

        if (Input.GetButtonDown("Fire1"))
            Shot(Input.GetKey(KeyCode.Q));
        
        if (Input.GetButtonDown("Fire2"))
            ShotGrenade();

    }

    private bool CanViewChange(float vertical)
    {
        float gunXangle = gun.transform.rotation.eulerAngles.x;
        float newXangle = gunXangle + vertical;
        return (newXangle < 40) || (newXangle > 330);
    }

    private void Shot(bool track)
    {
        GameObject bullet = Instantiate(bulletPrefab, muzzle.transform.position, muzzle.transform.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddRelativeForce(0, 0, 100);
        if(track)
        {
            BulletScript bs = bullet.GetComponent<BulletScript>();
            bs.TrackBullet();
        }
        
    }
    
    private void ShotGrenade()
    {
        Rigidbody rb = Instantiate(grenadePrefab, muzzle.transform.position, muzzle.transform.rotation).GetComponent<Rigidbody>();
        rb.AddRelativeForce(0, 0, 100);
    }

}
